# FLA = Fiduciary License Agreement, the legal form
# FRP = Licence Modification Guidelines, referred in the FLA
#       as FLA Relicencing Policy (FRP)
all : FLA.pdf FRP.pdf FLA-prefab.pdf

SRCS=kde.sty \
	preamble.tex \
	purpose.tex \
	parties.tex \
	grant.tex \
	grant-trailer.tex \
	rights.tex \
	trailer.tex


FLA.pdf : $(SRCS) grant-blank.tex FLA.tex
	pdflatex FLA
	pdflatex FLA

FLA-prefab.pdf : $(SRCS) grant-prefab.tex FLA-prefab.tex
	pdflatex FLA-prefab
	pdflatex FLA-prefab

FRP.pdf : kde.sty preamble.tex FRP.tex
	pdflatex FRP
	pdflatex FRP

